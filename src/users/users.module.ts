import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { ControllersController } from './controllers/controllers.controller';
import { ServicesService } from './services/services.service';
import { User } from 'src/typeorm/entities/user';
import { Profile } from 'src/typeorm/entities/profile';
import { Post } from 'src/typeorm/entities/post';

@Module({
  imports:[TypeOrmModule.forFeature([User, Profile, Post])],
  controllers: [ControllersController],
  providers: [ServicesService]
})
export class UsersModule {}
