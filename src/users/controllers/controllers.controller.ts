import { Body, Controller, Get, Post, Put, Delete, Param, ParseIntPipe } from '@nestjs/common';
import { CreateUserDto } from '../dtos/createUser.dto';
import { ServicesService } from '../services/services.service';
import { UpdateUserDto } from '../dtos/updateUser.dto';
import { CreateUserProfileDto } from '../dtos/createUserProfile.dto';
import { CreateUserPostDto } from '../dtos/createUserPost.dto';

@Controller('users')
export class ControllersController {
    constructor(private userService: ServicesService){}

    @Get()
    getUsers(){
        return this.userService.findUsers();
    }

    @Get('/related')
    getUsersWithRelation(){
        return this.userService.findUsersWithRelation();
    }

    @Post()
    createUser(@Body() createUserDto: CreateUserDto){
        return this.userService.createUser(createUserDto);
    }

    @Put(':id')
    async getUserById(
        @Param('id', ParseIntPipe) id: number, 
        @Body() updateUserDto: UpdateUserDto
    ){
        await this.userService.updateUser(id, updateUserDto);
    }

    @Delete(':id')
    async deleteUserById(
        @Param('id', ParseIntPipe) id: number,
    ){
        await this.userService.deleteUser(id);
    }

    @Post(':id/profiles')
    createUserProfile(
        @Param('id', ParseIntPipe) id: number, 
        @Body() createUserProfileDto: CreateUserProfileDto
    ){
        return this.userService.createUserProfile(id, createUserProfileDto);
    }

    @Post(':id/posts')
    createUserPost(@Param('id', ParseIntPipe) id: number, @Body() createUserPostDto: CreateUserPostDto){
        return this.userService.createUserPost(id, createUserPostDto);
    }
}
