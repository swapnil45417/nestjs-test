import {
    Column,
    Entity,
    JoinColumn,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Profile } from './profile';
import { Post } from './post';
  

@Entity({ name:'users' })
export class User{
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ unique:true })
    username: string;

    @Column()
    password: string;

    @Column()
    createdAt: Date;

    @OneToOne(() => Profile)
    @JoinColumn()
    profile: Profile;

    @OneToMany(() => Post, (post) => post.user)
    posts: Post[];
}