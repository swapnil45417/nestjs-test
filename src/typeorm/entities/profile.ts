import {
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
  

@Entity({ name:'user_profiles' })
export class Profile{
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({})
    firstname: string;

    @Column()
    lastname: string;

    @Column()
    age: number;

    @Column()
    dob: string;
}